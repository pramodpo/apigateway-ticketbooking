package com.usk.user.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class UserTicketResponse {
	int ticketId;
	int userId;
	String source;
	String destination;
	int noOfSeats;
	int amount;
	LocalDate transDate;

	public int getTicketId() {
		return ticketId;
	}

	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}
	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public int getNoOfSeats() {
		return noOfSeats;
	}

	public LocalDate getTransDate() {
		return transDate;
	}

	public void setTransDate(LocalDate transDate) {
		this.transDate = transDate;
	}
	public void setNoOfSeats(int noOfSeats) {
		this.noOfSeats = noOfSeats;
	}
	private List<UserPassengerResponse> passengerList = new ArrayList<UserPassengerResponse>();

	public List<UserPassengerResponse> getPassengerList() {
		return passengerList;
	}

	public void setPassengerList(List<UserPassengerResponse> passengerList) {
		this.passengerList = passengerList;
	}


}
