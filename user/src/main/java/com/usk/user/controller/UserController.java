package com.usk.user.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


import com.usk.user.dto.RequestTicketDto;

import com.usk.user.entity.User;
import com.usk.user.dto.UserTicketResponse;
import com.usk.user.service.UserService;



@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserService userService;
	
	
	@Autowired
    RestTemplate restTemplate;

	@GetMapping("/loaduserbyusername")
	public User getUserByUserName(@RequestParam String username) {
		return userService.getUserByUserName(username);
	}

	@GetMapping("/byName")
	public User findByUsername(@RequestParam String username) {
		return userService.findByUsername(username);
	}

	@PostMapping("/bookTicket")
	public String bookTicket(@RequestBody RequestTicketDto ticketDto) {
		
		  HttpHeaders headers = new HttpHeaders();
		  headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		  HttpEntity<RequestTicketDto> request = new HttpEntity<RequestTicketDto>(ticketDto, headers);
		  return restTemplate.exchange("http://localhost:8082/ticket/booking/ticketBooking", HttpMethod.POST, request, String.class).getBody();
	}
	@GetMapping("/getTransactionHistory/{userId}")
	public ArrayList<UserTicketResponse> getLastTransactionHistory(@PathVariable int userId) {
		 
		//RestTemplate restTemplate = new RestTemplate();
		Map<String, Integer> params = new HashMap<String, Integer>();
		params.put("userId", userId);
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		ResponseEntity<ArrayList<UserTicketResponse>> response = restTemplate.exchange("http://localhost:8082/ticket/booking/getTransactionHistory/{userId}", HttpMethod.GET,null,new ParameterizedTypeReference<ArrayList<UserTicketResponse>>() {},params);
		return response.getBody();
     }

	@GetMapping("/getLastTransactionDetails/{userId}")
	public ArrayList<UserTicketResponse> getLastTransactionDetails(@PathVariable int userId) {
		//return (ArrayList<UserTicketResponse>) trainService.getLastTransactionDetails(userId);
		Map<String, Integer> params = new HashMap<String, Integer>();
		params.put("userId", userId);
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		ResponseEntity<ArrayList<UserTicketResponse>> response = restTemplate.exchange("http://localhost:8082/ticket/booking/getLastTransactionDetails/{userId}", HttpMethod.GET,null,new ParameterizedTypeReference<ArrayList<UserTicketResponse>>() {},params);
		return response.getBody();
	}		 
	
}
