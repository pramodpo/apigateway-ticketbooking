package com.usk.ticketbooking.controller;


import java.time.LocalDate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.aspectj.lang.annotation.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
//import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.mockito.junit.MockitoJUnitRunner;

import com.usk.ticketbooking.model.*;

import com.usk.ticketbooking.service.TrainService;


@WebMvcTest(value = TicketBookingController.class)


@RunWith(MockitoJUnitRunner.class)
public class TicketBookingControllerTest {

	
	private MockMvc mockMvc;

	@InjectMocks
	TicketBookingController ticketBookingController;
	@Mock
	private TrainService trainService;
	
	@Before(value = "")
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(ticketBookingController).build();
	}

	LocalDate date1=LocalDate.of(2021, 03, 03);
	Transaction mockCourse = new Transaction(10, 2,"howrah","chennai",2,1400,date1);
	
	
	//Transaction mockCourse1 = new Transaction(10, 2,"howrah","chennai",2,1400,date1,Lis(1,"anu",35,"female","upper"));
	//String exampleCourseJson = "{\"ticketId\":\"userId\",\"source\":\"10Steps\",\"steps\":[\"Learn Maven\",\"Import Project\",\"First Example\",\"Second Example\"]}";

	@Test
	public void getLastTransactionDetails1() throws Exception {

		Mockito.when(
				trainService.getLastTransactionDetails1(Mockito.anyInt())).thenReturn(mockCourse);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/getLastTransactionDetails1/2").accept(
				MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		System.out.println(result.getResponse());
		String expected = "{ticketId:8,userId:2,source:howrah,destination:chennai,noOfSeats:2,amount:1400,transDate:2021-03-03}";

		JSONAssert.assertEquals(expected, result.getResponse()
				.getContentAsString(), false);
	}
	public void mockMvctest() throws Exception {

		ArrayList<Transaction> tickets = new ArrayList<>();
		ArrayList<Passenger> passList=new ArrayList<>();

		
		Passenger pass1=new Passenger();
		pass1.setPassengerId(11);
		pass1.setTicketId(9);
		pass1.setPassengerName("anu");
		pass1.setGender("female");
		pass1.setBerthPrefer("upper");
		pass1.setAge(40);
		passList.add(pass1);
		
		Transaction trans = new Transaction();
		trans.setTicketId(9);
		trans.setSource("chennai");
		trans.setDestination("howrah");
		trans.setNoOfSeats(1);
		trans.setUserId(2);
		trans.setAmount(1400);
		trans.setTransDate(date1);
		trans.setPassengerList(passList);
		tickets.add(trans);
		Transaction trans1 = new Transaction();
		trans.setTicketId(9);
		trans.setSource("chennai");
		trans.setDestination("howrah");
		trans.setNoOfSeats(1);
		trans.setUserId(2);
		trans.setAmount(1400);
		trans.setTransDate(date1);
		trans.setPassengerList(passList);
		tickets.add(trans1);
		
		
		
		Mockito.when(trainService.getLastTransactionDetails(2)).thenReturn(tickets);

		MvcResult response = mockMvc.perform(MockMvcRequestBuilders.get("/getLastTransactionDetails/2")).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		String content = response.getResponse().getContentAsString();
	}


	
	

}