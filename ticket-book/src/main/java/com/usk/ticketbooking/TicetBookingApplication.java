package com.usk.ticketbooking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class TicetBookingApplication {

	public static void main(String[] args) {
		SpringApplication.run(TicetBookingApplication.class, args);
	}

}
