package com.usk.ticketbooking.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.usk.ticketbooking.dto.RequestTicketDto;
import com.usk.ticketbooking.model.Passenger;
import com.usk.ticketbooking.model.Trains;
import com.usk.ticketbooking.model.Transaction;
import com.usk.ticketbooking.repository.PassengerRepository;
import com.usk.ticketbooking.repository.TrainsRepository;
import com.usk.ticketbooking.repository.TransactionRepository;

@Service
public class TrainServiceImpl implements TrainService {

	@Autowired
	TransactionRepository transRepository;
	@Autowired
	TrainsRepository tranisRepository;
	@Autowired
	PassengerRepository passengerRepository;

	public String bookTicket(RequestTicketDto ticketDto) {

		try {
			LocalDate date = LocalDate.now();
			Trains train = getTicketAvailable(ticketDto.getSource(), ticketDto.getDestination());
			if (train.getTicketAvailable() < ticketDto.getNoOfSeats()) {
				return "Ticket Not Confirmed because of no seats available";
			} else {
				int amount = train.getAmount() * ticketDto.getNoOfSeats();
				Transaction trans = new Transaction();
				trans.setUserId(ticketDto.getUserId());
				trans.setSource(ticketDto.getSource());
				trans.setDestination(ticketDto.getDestination());
				trans.setNoOfSeats(ticketDto.getNoOfSeats());
				trans.setTransDate(date);
				trans.setAmount(amount);
				int ticketId = transRepository.save(trans).getTicketId();
				List<Passenger> passengerList = new ArrayList<Passenger>();
				passengerList = processData(ticketDto.getPassengerList(), ticketId);
				passengerRepository.saveAll(passengerList);

			}

		}
		
		catch (Exception e) {

			e.printStackTrace();
		}
		return "Ticket booked and Confirmed";

	}

	public ArrayList<Transaction> getTransactionHistory(int userId) {

		ArrayList<Transaction> ticketHistory = transRepository.getTransactionHistory(userId);
		try {
			for (Transaction ticket : ticketHistory) {
				List<Passenger> passengerList = getPassengerList(ticket.getTicketId());
				ticket.setPassengerList(passengerList);
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return ticketHistory;
	}

	public Trains getTicketAvailable(String source, String destination) {
		Trains train = tranisRepository.findBySourceAndDestination(source, destination);
		return train;

	}

	public List<Passenger> getPassengerList(int ticketId) {
		return passengerRepository.getPassengerList(ticketId);
	}

	@Transactional
	public List<Passenger> processData(List<Passenger> passList, int ticketId) {
		List<Passenger> passenList = new ArrayList<Passenger>();
		try {
			for (Passenger entity : passList) {
				entity.setTicketId(ticketId);
				passengerRepository.save(entity);
				passenList.add(entity);
			}
		}

		catch (Exception e) {
			e.printStackTrace();
		}
		return passenList;
	}

	public ArrayList<Transaction> getLastTransactionDetails(int userId) {

		ArrayList<Transaction> ticketHistory = transRepository.getLatTransactionDetails(userId);

		try {

			for (Transaction ticket : ticketHistory) {
				List<Passenger> passengerList = getPassengerList(ticket.getTicketId());
				ticket.setPassengerList(passengerList);
			}
		}

		catch (Exception e) {
			e.printStackTrace();
		}
		return ticketHistory;
	}

	public Transaction getLastTransactionDetails1(int userId) {
		return transRepository.getLatTransactionDetails1(userId);
	}

}
