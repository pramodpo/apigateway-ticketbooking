package com.usk.ticketbooking.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.usk.ticketbooking.dto.RequestTicketDto;
import com.usk.ticketbooking.model.Trains;
import com.usk.ticketbooking.model.Transaction;
import com.usk.ticketbooking.repository.TransactionRepository;

@Service
public interface TrainService {

	public String bookTicket(RequestTicketDto ticketDto);

	public ArrayList<Transaction> getTransactionHistory(int userId);
	
	public Trains getTicketAvailable(String source,String destination);
	
	public ArrayList<Transaction> getLastTransactionDetails(int userId);
	
	public Transaction getLastTransactionDetails1(int userId);

}
