package com.usk.ticketbooking.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.usk.ticketbooking.model.Passenger;



public interface PassengerRepository extends JpaRepository<Passenger, Integer>{
	
	@Query(value = "SELECT * FROM Passenger WHERE ticket_id=:ticketId", nativeQuery = true)
	ArrayList<Passenger> getPassengerList(@Param("ticketId") int ticketId);
	
}
