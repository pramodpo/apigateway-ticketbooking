package com.usk.ticketbooking.repository;


import java.util.ArrayList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.usk.ticketbooking.model.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Integer> {
	
	@Query(value = "SELECT * FROM transaction WHERE user_id=:userId", nativeQuery = true)
	ArrayList<Transaction> getTransactionHistory(@Param("userId") int userId);
	
	@Query(value = "SELECT * FROM transaction WHERE user_id=:userId and trans_date in (select MAX(trans_date) from transaction GROUP BY user_id ORDER BY MAX(trans_date)) ", nativeQuery = true)
	ArrayList<Transaction> getLatTransactionDetails(@Param("userId") int userId);
	
	//tranc_date IN (SELECT MAX(tranc_date)FROM cus_tranc GROUP BY cus_id);

	@Query(value = "SELECT * FROM transaction WHERE user_id=:userId and trans_date in (select MAX(trans_date) from transaction GROUP BY user_id ORDER BY MAX(trans_date)) ", nativeQuery = true)
	Transaction getLatTransactionDetails1(@Param("userId") int userId);
	
}
