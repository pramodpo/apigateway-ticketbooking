package com.usk.ticketbooking.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.usk.ticketbooking.model.Trains;
import com.usk.ticketbooking.model.Transaction;

public interface TrainsRepository extends JpaRepository<Trains, Integer>  {

	Trains findBySourceAndDestination(String source,String destination);
}
