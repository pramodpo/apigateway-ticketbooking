package com.usk.ticketbooking.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.usk.ticketbooking.model.Passenger;

public class RequestTicketDto {
	@NotNull
	int userId;
	@NotNull
	@Size(min=3, message="source should have atleast 3 characters")
	String source;
	@NotNull
	@Size(min=3, message="destination should have atleast 3 characters")
	String destination;
	@NotNull
	int noOfSeats;
	LocalDate transDate;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public int getNoOfSeats() {
		return noOfSeats;
	}

	public void setNoOfSeats(int noOfSeats) {
		this.noOfSeats = noOfSeats;
	}
	public LocalDate getTransDate() {
		return transDate;
	}

	public void setTransDate(LocalDate transDate) {
		this.transDate = transDate;
	}
	private List<Passenger> passengerList = new ArrayList<Passenger>();

	public List<Passenger> getPassengerList() {
		return passengerList;
	}

	public void setPassengerList(List<Passenger> passengerList) {
		this.passengerList = passengerList;
	}

	
	

}
