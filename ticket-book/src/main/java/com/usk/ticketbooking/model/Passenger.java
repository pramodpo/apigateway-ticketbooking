package com.usk.ticketbooking.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
@Entity
@Table
public class Passenger {


@Id
@Column(name = "passengerId")
@GeneratedValue(strategy = GenerationType.IDENTITY)
private int passengerId;

@Column(name = "ticketId")
int ticketId;

public int getTicketId() {
	return ticketId;
}
public void setTicketId(int ticketId) {
	this.ticketId = ticketId;
}
@Column(name = "passengerName")
@Size(min = 3, message = "passengerName should have atleast 3 characters")
String passengerName;

@Column(name = "age")
int age;
@Column(name = "gender")
String gender;

@Column(name = "berthPrefer")
String berthPrefer;

public int getPassengerId() {
	return passengerId;
}
public void setPassengerId(int passengerId) {
	this.passengerId = passengerId;
}
public String getPassengerName() {
	return passengerName;
}
public void setPassengerName(String passengerName) {
	this.passengerName = passengerName;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
public String getBerthPrefer() {
	return berthPrefer;
}
public void setBerthPrefer(String berthPrefer) {
	this.berthPrefer = berthPrefer;
}



}
