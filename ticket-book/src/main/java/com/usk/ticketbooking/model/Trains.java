package com.usk.ticketbooking.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table
public class Trains {

	@Id
	@Column(name = "trainId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int trainId;

	public int getTrainId() {
		return trainId;
	}

	public void setTrainId(int trainId) {
		this.trainId = trainId;
	}

	@Column(name = "trainName")
	@NotNull
	@Size(min=3, message="Customer Name should have atleast 3 characters")
	private String trainName;

	public String getTrainName() {
		return trainName;
	}

	public void setTrainrName(String trainName) {
		this.trainName = trainName;
	}

	@Column(name = "source")
	@Size(min=10, message="Contact No should have atleast 10 characters")
	private String source;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@Column(name = "destination")
	@Size(min=10, message="Contact No should have atleast 10 characters")
	private String destination;
	
	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}
	
	@Column(name = "ticketAvailable")
	@Size(min=10, message="Contact No should have atleast 10 characters")
	private int ticketAvailable;
	
	public int getTicketAvailable() {
		return ticketAvailable;
	}
	
	public void setTicketAvailable(int ticketAvailable) {
		this.ticketAvailable = ticketAvailable;
	}
	

	@Column(name = "amount")
	@Size(min=10, message="Contact No should have atleast 10 characters")
	private int amount;

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	
		
	/*
	 * @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy =
	 * "trains") private Transaction transaction;
	 */

}
