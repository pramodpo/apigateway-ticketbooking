package com.usk.ticketbooking.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

@Entity
@Table
public class Transaction {

	@Id
	@Column(name = "ticketId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int ticketId;

	public int getTicketId() {
		return ticketId;
	}

	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}

	@Column(name = "userId")
	private int userId;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Column(name = "source")
	@Size(min = 3, message = "Contact No should have atleast 3 characters")
	private String source;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@Column(name = "destination")
	@Size(min = 3, message = "Contact No should have atleast 3 characters")
	private String destination;

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	@Column(name = "noOfSeats")
	private int noOfSeats;

	public int getNoOfSeats() {
		return noOfSeats;
	}

	public void setNoOfSeats(int noOfSeats) {
		this.noOfSeats = noOfSeats;
	}
	
	@Column(name = "amount")
	private int amount;

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	@Column(name="trans_date")
	private LocalDate transDate;
	/*
	 * @Column(name = "accountNo")
	 * 
	 * @Size(min=11, message="Account No should have atleast 11 characters") private
	 * String accountNo;
	 * 
	 * @OneToOne(fetch = FetchType.LAZY, optional = false)
	 * 
	 * @JoinColumn(name = "customerId", nullable = false) private Customer customer;
	 */

	public LocalDate getTransDate() {
		return transDate;
	}

	public void setTransDate(LocalDate transDate) {
		this.transDate = transDate;
	}
	@Transient
	private List<Passenger> passengerList = new ArrayList<Passenger>();

	public List<Passenger> getPassengerList() {
		return passengerList;
	}

	public void setPassengerList(List<Passenger> passengerList) {
		this.passengerList = passengerList;
	}
	public Transaction() {
	}
	
	public Transaction(int ticketId, int userId, String source, String destination, int noOfSeats,int amount,LocalDate transDate,List<Passenger> passengerList) {
		this.ticketId = ticketId;
		this.userId = userId;
		this.source = source;
		this.destination = destination;
		this.noOfSeats = noOfSeats;
		this.amount=amount;
		this.transDate=transDate;
		this.passengerList=passengerList;

	}
	public Transaction(int ticketId, int userId, String source, String destination, int noOfSeats,int amount,LocalDate transDate) {
		this.ticketId = ticketId;
		this.userId = userId;
		this.source = source;
		this.destination = destination;
		this.noOfSeats = noOfSeats;
		this.amount=amount;
		this.transDate=transDate;
		

	}

}
