package com.usk.ticketbooking.controller;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usk.ticketbooking.dto.RequestTicketDto;
import com.usk.ticketbooking.model.Transaction;
import com.usk.ticketbooking.service.TrainService;

@RestController
@RequestMapping("/booking")
public class TicketBookingController {

	@Autowired
	Environment environment;

	@Autowired
	TrainService trainService;

	@PostMapping("/port")
	public String getPortNo() {
		String port = environment.getProperty("local.server.port");
		return "From ticket-booking app : " + port;
	}

	@PostMapping("/ticketBooking")
	public String bookTickets(@Valid @RequestBody RequestTicketDto ticketDto) {
		return trainService.bookTicket(ticketDto);

	}

	@GetMapping("/getTransactionHistory/{userId}")
	public ArrayList<Transaction> getTransactionHistory(@PathVariable int userId) {
		return (ArrayList<Transaction>) trainService.getTransactionHistory(userId);

	}
	@GetMapping("/getLastTransactionDetails/{userId}")
	public ArrayList<Transaction> getLastTransactionDetails(@PathVariable int userId) {
		return (ArrayList<Transaction>) trainService.getLastTransactionDetails(userId);

	}
	@GetMapping("/getLastTransactionDetails1/{userId}")
	public Transaction getLastTransactionDetails1(@PathVariable int userId) {
		return trainService.getLastTransactionDetails1(userId);

	}

}
