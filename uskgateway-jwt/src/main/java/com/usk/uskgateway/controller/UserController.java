package com.usk.uskgateway.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.usk.uskgateway.dto.RequestTicketDto;
import com.usk.uskgateway.feignclient.UserCLient;
import com.usk.uskgateway.model.ApiResponse;
import com.usk.uskgateway.model.User;
import com.usk.uskgateway.model.UserDto;
import com.usk.uskgateway.dto.UserTicketResponse;
import com.usk.uskgateway.service.impl.UserServiceImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserServiceImpl userService;
	@Autowired
	private UserCLient userCLient;
    
    @PostMapping
    public ApiResponse<User> saveUser(@RequestBody UserDto user){
        return new ApiResponse<>(HttpStatus.OK.value(),"User saved successfully.",userService.save(user));
    }
    
	
    @PostMapping("/bookTicket") 
	public String bookTicket(@RequestBody RequestTicketDto ticketDto) {
      	return userCLient.bookTicket(ticketDto);  
	}
	
	
	@GetMapping("/getTransactionHistory/{userId}")
	public ArrayList<UserTicketResponse> getLastTransactionHistory(@PathVariable int userId)
	{  
	if(userId==0)
		 throw new IllegalArgumentException();
	  return userCLient.getTransactionHistory(userId); 
	  }
	  
	@GetMapping("/getLastTransactionDetails/{userId}")
	public ArrayList<UserTicketResponse> getLastTransactionDetails(@PathVariable int userId)
	{  
	  if(userId==0)
		  throw new IllegalArgumentException();
	  return userCLient.getLastTransactionDetails(userId); 
	  }
	
		
	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<Object> handleException(IllegalArgumentException e){
		
		return new ResponseEntity<Object>("illegal arg exception in controller", HttpStatus.BAD_REQUEST);
	}
 }
