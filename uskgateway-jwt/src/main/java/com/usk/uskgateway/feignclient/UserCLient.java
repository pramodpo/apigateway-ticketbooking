package com.usk.uskgateway.feignclient;

import java.util.ArrayList;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.usk.uskgateway.dto.RequestTicketDto;
import com.usk.uskgateway.model.User;
import com.usk.uskgateway.dto.UserTicketResponse;

import feign.Param;

@FeignClient(name ="user-simple",  url = "http://localhost:8081/user")
public interface UserCLient {

	@GetMapping("/loaduserbyusername")
	public User getUserByUserName(@Param("username") @RequestParam	String username);

	@GetMapping("/byName")
	public User findByUsername(@Param("username") @RequestParam String username);
	
	@PostMapping("/bookTicket")
	public String bookTicket(@RequestBody RequestTicketDto ticketDto);
	
	
	@GetMapping("/getTransactionHistory/{userId}")
	public ArrayList<UserTicketResponse> getTransactionHistory(@PathVariable int userId);
    
	@GetMapping("/getLastTransactionDetails/{userId}")
	public ArrayList<UserTicketResponse> getLastTransactionDetails(@PathVariable int userId);
    
	
}
