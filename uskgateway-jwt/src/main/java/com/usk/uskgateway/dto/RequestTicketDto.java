package com.usk.uskgateway.dto;

import java.util.ArrayList;
import java.util.List;

import com.usk.uskgateway.dto.RequestPassengerDto;

public class RequestTicketDto {

	//int ticketId;
	int userId;
	String source;
	String destination;
	int noOfSeats;
	

	/*
	 * public int getTicketId() { return ticketId; } public void setTicketId(int
	 * ticketId) { this.ticketId = ticketId; }
	 */
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public int getNoOfSeats() {
		return noOfSeats;
	}
	public void setNoOfSeats(int noOfSeats) {
		this.noOfSeats = noOfSeats;
	}
	private List<RequestPassengerDto> passengerList = new ArrayList<RequestPassengerDto>();

	public List<RequestPassengerDto> getPassengerList() {
		return passengerList;
	}

	public void setPassengerList(List<RequestPassengerDto> passengerList) {
		this.passengerList = passengerList;
	}
	
	
}
