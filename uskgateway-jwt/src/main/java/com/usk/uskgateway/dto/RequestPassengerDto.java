package com.usk.uskgateway.dto;

public class RequestPassengerDto {
	int ticketId;
	public int getTicketId() {
		return ticketId;
	}
	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}
	public String getPassengerName() {
		return passengerName;
	}
	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBerthPrefer() {
		return berthPrefer;
	}
	public void setBerthPrefer(String berthPrefer) {
		this.berthPrefer = berthPrefer;
	}
	String passengerName;
	int age;
	String gender;
	String berthPrefer;
}
